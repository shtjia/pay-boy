/**
 * 全局异常处理
 */
// 全局ajax异常处理
function errorCallBack(response, ajaxOptions, thrownError) {
	if (response.status == 401) {
		layer.alert('session已过期，请重新登录！',{icon:2})
		window.location.href = "/index"; // 跳转到首页因为没权限会重定向到登录页
		return;
	}
	if (response.status == 403) {
		layer.alert('没有权限进行该操作！',{icon:2})
		return;
	}
	if (response.status == 507) {
		layer.alert('访问过于频繁，请稍后再试！',{icon:2})
		return;
	}
}

/**
 * 日期相关
 */
// 将指定的毫秒数加到此实例的值上
Date.prototype.addMilliseconds = function(value) {
    var millisecond = this.getMilliseconds();
    this.setMilliseconds(millisecond + value);
    return this;
};
// 将指定的秒数加到此实例的值上
Date.prototype.addSeconds = function(value) {
    var second = this.getSeconds();
    this.setSeconds(second + value);
    return this;
};
// 将指定的分钟数加到此实例的值上
Date.prototype.addMinutes = function(value) {
    var minute = this.addMinutes();
    this.setMinutes(minute + value);
    return this;
};
// 将指定的小时数加到此实例的值上
Date.prototype.addHours = function(value) {
    var hour = this.getHours();
    this.setHours(hour + value);
    return this;
};
// 将指定的天数加到此实例的值上
Date.prototype.addDays = function(value) {
    var date = this.getDate();
    this.setDate(date + value);
    return this;
};
// 将指定的星期数加到此实例的值上
Date.prototype.addWeeks = function(value) {
    return this.addDays(value * 7);
};
// 将指定的月份数加到此实例的值上
Date.prototype.addMonths = function(value) {
    var month = this.getMonth();
    this.setMonth(month + value);
    return this;
};
// 将指定的年份数加到此实例的值上
Date.prototype.addYears = function(value) {
    var year = this.getFullYear();
    this.setFullYear(year + value);
    return this;
};
// 格式化日期显示 format="yyyy-MM-dd hh:mm:ss";
Date.prototype.format = function(format) {
    var o = {
        "M+": this.getMonth() + 1,
        // month
        "d+": this.getDate(),
        // day
        "h+": this.getHours(),
        // hour
        "m+": this.getMinutes(),
        // minute
        "s+": this.getSeconds(),
        // second
        "q+": Math.floor((this.getMonth() + 3) / 3),
        // quarter
        "S": this.getMilliseconds() // millisecond
    }
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}

/**
 * JSON序列化相关
 */
$.fn.serializeJson = function() {
    var serializeObj = {};
    var array = this.serializeArray();
    var str = this.serialize();
    $(array).each(function() {
        if (serializeObj[this.name]) {
            if ($.isArray(serializeObj[this.name])) {
                serializeObj[this.name].push(this.value);
            } else {
                serializeObj[this.name] = [serializeObj[this.name], this.value];
            }
        } else {
            serializeObj[this.name] = this.value;
        }
    });
    return serializeObj;
};

/**
 * 将数值四舍五入(保留2位小数)后格式化成金额形式
 * @return 金额格式的字符串,如'1,234,567.45'
 */
function common_utils_format_currency(num) {
    num = num.toString().replace(/\$|\,/g,'');
    if(isNaN(num))
    num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();
    if(cents<10)
    cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+
    num.substring(num.length-(4*i+3));
    return (((sign)?'':'-') + num + '.' + cents);
}

/**
 * 将日期字符串yyyy-MM-dd HH:mm:ss转换成date
 */
function common_utils_parse_datetime(dateString) { 
	var date = new Date(dateString).format("yyyy-MM-dd hh:mm:ss");
    if (date == "1970-01-01 08:00:00")
        return new Date();
    else
        return date;
}

/**
 * 将日期字符串yyyy-MM-dd转换成date
 */
function common_utils_parse_date(dateString) { 
	if (dateString) { 
		var date = new Date(dateString.replace(/-/,"/")) 
		return date;
	}
}

/**
 * 正则表达式相关
 */
$.validator.addMethod("stringCheck",function(value, element) {
    return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
},"只能包括中文字、英文字母、数字和下划线");

//用于判断昵称是字母、数字、特殊字符或“_”组成
$.validator.addMethod("isNickName", function(value, element) {
    var reg = /^[\w][\w@.]+$/;
	return this.optional(element) || reg.test(value);
}, "用户名必须由字母、数字或特殊符号(@_.)组成");

/**
 * 校验小程序地址
 */
$.validator.addMethod("miniUrlCheck",function(value, element) {
	return this.optional(element) || /^((\/[\w\-_]+)|([\w\-_]+))+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?$/.test(value);
},"请输入正确的小程序url");

// 中文字两个字节
$.validator.addMethod("byteRangeLength",function(value, element, param) {
    var length = value.length;
    for (var i = 0; i < value.length; i++) {
        if (value.charCodeAt(i) > 127) {
            length++;
        }
    }
    return this.optional(element) || (length >= param[0] && length <= param[1]);
},"请确保输入的值在3-15个字节之间(一个中文字算2个字节)");

// 手机号码验证
$.validator.addMethod("isMobile",function(value, element) {
    var length = value.length;
    var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
    return this.optional(element) || (length == 11 && mobile.test(value));
},"请正确填写您的手机号码");

// 邮箱验证
jQuery.validator.addMethod("isEmail", function(value, element) {
    var pattern =   /^[_a-zA-Z0-9\-\.]+@([\-_a-zA-Z0-9]+\.)+[a-zA-Z0-9]{2,3}$/;
    return this.optional(element) || (pattern.test(value));
}, "邮箱格式不正确");

// 电话号码验证
$.validator.addMethod("isTel",function(value, element) {
    var tel = /^\d{3,4}-?\d{7,9}$/; // 电话号码格式010-12345678
    return this.optional(element) || (tel.test(value));
},"请正确填写您的电话号码");

// 联系电话(手机/电话皆可)验证
$.validator.addMethod("isPhone",function(value, element) {
    var length = value.length;
    var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
    var tel = /^\d{3,4}-?\d{7,9}$/;
    return this.optional(element) || (tel.test(value) || mobile.test(value));
},"请正确填写您的联系电话");

// 邮政编码验证
$.validator.addMethod("isZipCode",function(value, element) {
    var tel = /^[0-9]{6}$/;
	return this.optional(element) || (tel.test(value));
},"请正确填写您的邮政编码");

$.validator.addMethod("isAmount", function(value, element, params) {
    var re = /^(0|[1-9]\d*)(\.\d{1,2})?$/;
    var result = re.test(value);
    return result;

}, "金额不能以0开头");

// IP地址验证   
$.validator.addMethod("ip", function(value, element) {    
	return this.optional(element) || /^(([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))\.)(([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))\.){2}([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))$/.test(value);    
}, "请填写正确的IP地址！");

//用于判断是否是给定数字的整数倍
$.validator.addMethod("times", function(value, element,param) {    
	return this.optional(element) || Number(value)%Number(param) == 0;    
}, $.validator.format("必须是{0}的整数倍！")); 

//用于判断金额是否过大，主体值>参数
$.validator.addMethod("noBiggerThan",function(value,element,param){
	return Number(value) <= Number(param);
},$.validator.format("金额太大！"));

//用于判断金额是否过小
$.validator.addMethod("nolitterThan",function(value,element,param){
    return Number(value) >= Number(param);
},$.validator.format("金额太小！"));

//用于判断是否是给定数字的整数倍
$.validator.addMethod("bigtimes", function(value, element,param) {    
  return this.optional(element) || Number(value) * 10000 % Number(param) == 0;    
}, $.validator.format("必须是{0}的整数倍！")); 

//用于判断一个输入框的值小于另外一个输入框的
$.validator.addMethod("smaller", function(value, element, param) {
    var target = $( param );
    return this.optional(element) || Number(value) <= Number(target.val());
}, $.validator.format("请正确填写"));

$.validator.addMethod("isNoChinese", function(value, element) {
    var reg = /[\u4E00-\u9FA5]|[\uFE30-\uFFA0]/gi;
    return this.optional(element) || (!reg.test(value) && /^[\u0391-\uFFE5\w]+$/.test(value)) ;
}, "用户名必须由字母、数字或“_”组成");

// 只能是中文字符
jQuery.validator.addMethod("isChinese", function(value, element) {
    var pattern = /^[\u4e00-\u9fa5]+$/;
    return this.optional(element) || (pattern.test(value));
}, "只能是中文字符");

// 只能是中文字符和标点符号
jQuery.validator.addMethod("isZHPunc", function(value, element) {
    var pattern = /^[\u4e00-\u9fa5]|[\（\）\《\》\——\；\，\。\“\”\<\>\！]$/;
    return this.optional(element) || (pattern.test(value));
}, "只能是中文字符和标点符号");

// 只能是英文字符
jQuery.validator.addMethod("isEnglish", function(value, element) {
    var pattern = /^[A-Za-z]+$/;
    return this.optional(element) || (pattern.test(value));
}, "只能是英文字符");

//用于判断是否含有空格
$.validator.addMethod("isNoSpace", function(value, element) {
    var reg = /^[^ ]+$/;
    return this.optional(element) || reg.test(value);
}, "密码中不能含有任何空格");

$.validator.addMethod("isValidBizUserId", function(value, element) {
    var reg = /[\u4E00-\u9FA5]|[\uFE30-\uFFA0]/gi;
    return this.optional(element) || (!reg.test(value) && /^[\u0391-\uFFE5\w]+$/.test(value)) ;
}, "引荐人工号格式不正确");

//密码复杂度校验
$.validator.addMethod("complexity", function(value, element) {  
    var reg = /^(?=.*\d)(?=.*[a-zA-Z])[\S]+$/;
    return this.optional(element) || reg.test(value);    
}, $.validator.format("密码过于简单，至少要字母和数字的组合"));

//登录密码复杂度校验
$.validator.addMethod("loginPwdComplexity", function(value, element) {  
    var reg = /^(?=.*\d)(?=.*[a-zA-Z])[\S]+$/;
    return this.optional(element) || reg.test(value);    
}, $.validator.format("登录密码为8~16位数字和字母的组合")); 

//金额校验
$.validator.addMethod("money", function(value, element) {  
    var reg = /^[0-9]+(.[0-9]{1,2})?$/;
	return this.optional(element) || reg.test(value);    
}, $.validator.format("格式有误，只允许输入两位小数")); 

$.validator.addMethod("yearRate", function(value, element) {  
    var reg = /^[0-9]+(.[0-9]{1})?$/;
	return this.optional(element) || reg.test(value);    
}, $.validator.format("格式有误，只允许输入1位小数")); 

//年龄 表单验证规则
$.validator.addMethod("age", function(value, element) {   
    var age = /^(?:[1-9][0-9]?|1[01][0-9]|120)$/;
    return this.optional(element) || (age.test(value));
}, "不能超过120岁"); 
///// 20-60   /^([2-5]\d)|60$/

//传真
$.validator.addMethod("fax",function(value,element){
    var fax = /^(\d{3,4})?[-]?\d{7,8}$/;
    return this.optional(element) || (fax.test(value));
},"传真格式如：0371-68787027");

//验证当前值和目标val的值相等 相等返回为 false
$.validator.addMethod("equalTo2",function(value, element){
    var returnVal = true;
    var id = $(element).attr("data-rule-equalto2");
    var targetVal = $(id).val();
    if(value === targetVal){
        returnVal = false;
    }
    return returnVal;
},"不能和原始密码相同");

//大于指定数
$.validator.addMethod("gt",function(value, element){
    var returnVal = false;
    var gt = $(element).data("gt");
    if(value > gt && value != ""){
        returnVal = true;
    }
    return returnVal;
},"不能小于0 或空");

//指定数字的整数倍
$.validator.addMethod("times", function (value, element) {
    var returnVal = true;
    var base=$(element).attr('data-rule-times');
    if(value%base!=0){
        returnVal=false;
    }
    return returnVal;
}, "必须是发布赏金的整数倍");

$.validator.addMethod("transferPrice", function(value, element,param) { 
    var account = (parseFloat(param) * 0.75).toString();
    var resultAccount;
    if(account.indexOf('.') >= 0){
        resultAccount = account.substring(0, account.indexOf('.') + 3)
    }

    if(parseFloat(value) < parseFloat(resultAccount)){
        return false;
    }else{
        return true;
    }
}, $.validator.format("转让价不能小于剩余本金的75%")); 

$.validator.addMethod("transferMaxPrice", function(value, element,param) {  
    if(parseFloat(value) > parseFloat(param)){
        return false;
    }else{
        return true;
    }
}, $.validator.format("转让价不能大于剩余本金")); 

$.validator.addMethod("notEqualTo", function(value, element, param) {
    var target = $( param );
    alert(target.val())
    return this.optional(element) || Number(value) != Number(target.val());
});

//身份证号码验证
$.validator.addMethod("isIdCardNo", function(value, element) { 
	return this.optional(element) || isIdCardNo(value);
}, "身份证号错误(不区分大小写)"); 

//增加身份证验证
function isIdCardNo(num) {
    var factorArr = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1);
    var parityBit = new Array("1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2");
    var parityBit1 = new Array("1", "0", "x", "9", "8", "7", "6", "5", "4", "3", "2");
    var varArray = new Array();
    var intValue;
    var lngProduct = 0;
    var intCheckDigit;
    var intStrLen = num.length;
    var idNumber = num;
    // initialize
    if ((intStrLen != 15) && (intStrLen != 18)) {
        return false;
    }
    // check and set value
    for (i = 0; i < intStrLen; i++) {
        varArray[i] = idNumber.charAt(i);
        if ((varArray[i] < '0' || varArray[i] > '9') && (i != 17)) {
            return false;
        } else if (i < 17) {
            varArray[i] = varArray[i] * factorArr[i];
        }
    }
    if (intStrLen == 18) {
        //check date
        var date8 = idNumber.substring(6, 14);
        if (isDate8(date8) == false) {
            return false;
        }
        // calculate the sum of the products
        for (i = 0; i < 17; i++) {
            lngProduct = lngProduct + varArray[i];
        }
        // calculate the check digit
        intCheckDigit = parityBit[lngProduct % 11];
        intCheckDigit1 = parityBit1[lngProduct % 11];
        // check last digit
        if (varArray[17] != intCheckDigit && varArray[17] != intCheckDigit1) {
            return false;
        }
    }
    else {        //length is 15
        //check date
        var date6 = idNumber.substring(6, 12);
        if (isDate6(date6) == false) {
            return false;
        }
    }
    return true;
}
function isDate6(sDate) {
    if (!/^[0-9]{6}$/.test(sDate)) {
        return false;
    }
    var year, month, day;
    year = sDate.substring(0, 4);
    month = sDate.substring(4, 6);
    if (year < 1700 || year > 2500) return false
    if (month < 1 || month > 12) return false
    return true
}
/**
* 判断是否为"YYYYMMDD"式的时期
*/
function isDate8(sDate) {
    if (!/^[0-9]{8}$/.test(sDate)) {
        return false;
    }
    var year, month, day;
    year = sDate.substring(0, 4);
    month = sDate.substring(4, 6);
    day = sDate.substring(6, 8);
    var iaMonthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if (year < 1700 || year > 2500) return false
    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) iaMonthDays[1] = 29;
    if (month < 1 || month > 12) return false
    if (day < 1 || day > iaMonthDays[month - 1]) return false
    return true
}
